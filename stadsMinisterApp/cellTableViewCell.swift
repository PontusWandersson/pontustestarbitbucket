//
//  cellTableViewCell.swift
//  stadsMinisterApp
//
//  Created by Pontus Andersson on 2016-10-25.
//  Copyright © 2016 Pontus Andersson. All rights reserved.
//

import UIKit

class cellTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var ageLabel: UILabel!
        
    @IBOutlet weak var theImage: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        theImage.layer.cornerRadius = 35
        theImage.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
