//
//  ViewController.swift
//  stadsMinisterApp
//
//  Created by Pontus Andersson on 2016-10-25.
//  Copyright © 2016 Pontus Andersson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var theImage: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var partiLabel: UILabel!
    
    
    var person = personClass()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
   
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        titleLabel.text = person.namesOfMinister
        descriptionLabel.text = person.descriptionOfMinister
        theImage.image = UIImage(named: person.imageOfMinister)
        partiLabel.text = person.whatKindOfParti
        
        
        self.titleLabel.transform = CGAffineTransform(translationX: -view.frame.size.width, y: 5)
        
        UIView.animate(withDuration: 1.5, animations: {
            
            self.titleLabel.transform = CGAffineTransform(translationX: 0, y: 0)
            
        })
        
        
        
    }


        
        
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

