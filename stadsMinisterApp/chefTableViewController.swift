//
//  chefTableViewController.swift
//  stadsMinisterApp
//
//  Created by Pontus Andersson on 2016-10-25.
//  Copyright © 2016 Pontus Andersson. All rights reserved.
//

import UIKit

class chefTableViewController: UITableViewController {
    
    
    var people = [personClass]()
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        let p1 = personClass()
        p1.namesOfMinister = "Stefan Löfven"
        p1.ageBetweenOfMinister = "2014 - ???"
        p1.descriptionOfMinister = "Stefan heter jag och sysslar med metall... Skägg. Stefan Löfven föddes i Aspudden i Stockholm,5 men kom tio månader gammal som fosterbarn till Sunnersta i Sollefteå kommun, då hans biologiska mor inte kunde behålla båda sina barn"
        p1.imageOfMinister = "stefan.jpeg"
        p1.whatKindOfParti = "S"
        people.append(p1)

        let p2 = personClass()
        p2.namesOfMinister = "Fredrik Reinfeldt"
        p2.ageBetweenOfMinister = "2006 - 2014"
        p2.descriptionOfMinister = "Fredrik var jag inte döpt till, men jag kliade bort allt mitt hår på fötterna. Fredrik Reinfeldt föddes 1965 som äldsta barn i familjen, som då bodde i en lägenhet i Österhaninge, men flyttade strax därpå till London där fadern Bruno Reinfeldt under två år var konsult för Shell."
        p2.imageOfMinister = "fredrik.jpg"
        p2.whatKindOfParti = "M"
        people.append(p2)
        
        let p3 = personClass()
        p3.namesOfMinister = "Göran Persson"
        p3.ageBetweenOfMinister = "1996 - 2006"
        p3.descriptionOfMinister = "Göran tycker whiskey smakar guttet. Persson blev åter invald i riksdagen 1991, men förlorade sin post i regeringen eftersom socialdemokraterna samtidigt förlorade regeringsmakten. Under oppositionsåren 1991–1994 fick han en alltmer framträdande roll i partiet."
        p3.imageOfMinister = "goran.jpg"
        p3.whatKindOfParti = "S"
        people.append(p3)
        
        let p4 = personClass()
        p4.namesOfMinister = "Carl Bildt"
        p4.ageBetweenOfMinister = "1991 - 1994"
        p4.descriptionOfMinister = "JOOO, nu är de ju såhär att... Bildt tog studentexamen 1968 på Östra Real och blev samma år medlem i Moderaterna. Han gjorde värnplikt som handräckningsvärnpliktig personalassistent vid Försvarsstaben. "
        p4.imageOfMinister = "carl.jpg"
        p4.whatKindOfParti = "M"
        people.append(p4)
        
        let p5 = personClass()
        p5.namesOfMinister = "Ingvar Carlsson"
        p5.ageBetweenOfMinister = "1986 - 1991 & 1994 - 1996"
        p5.descriptionOfMinister = "Ingvar älskade att köra moppe som liten. Ingvar Carlsson är son till lagerarbetaren Olof Karlsson (1888–1947) och textilarbeterskan Ida Johansson (1893–1976) samt bror till två äldre bröder"
        p5.imageOfMinister = "ingvar.jpg"
        p5.whatKindOfParti = "S"
        people.append(p5)
        
        
        let p6 = personClass()
        p6.namesOfMinister = "Ola Ullsten"
        p6.ageBetweenOfMinister = "1978 - 1979"
        p6.descriptionOfMinister = "Efter socionomexamen i Stockholm blev Ullsten sekreterare i Folkpartiets riksdagsgrupp. Ullsten arbetade även i den amerikanske republikanen Nelson Rockefellers framgångsrika guvernörskampanj i New York. 1962–1964 var han ordförande i Folkpartiets Ungdomsförbund. I riksdagsvalet 1964 kandiderade han i Rösta ungt-kampanjen och invaldes i andra kammaren."
        p6.imageOfMinister = "ola.jpeg"
        p6.whatKindOfParti = "F"
        people.append(p6)
        
        let p7 = personClass()
        p7.namesOfMinister = "Thorbjörn Fälldin"
        p7.ageBetweenOfMinister = "1976 - 1978 & 1979 - 1982"
        p7.descriptionOfMinister = "Fälldin föddes i Högsjö socken i Ångermanland som son till lantbrukaren Nils Johan Fälldin (1893–1971) och Hulda Olsson (1900–1983). Han blev 1956 själv lantbrukare på gården, då han även gifte sig med Solveig Öberg (född 1935 i Skogs socken).[5] De har tre barn. Fälldin tog realexamen 1945 som privatist, reservofficersexamen 1951, var styrelseledamot i Svenska fåravelsföreningen 1959–68 och dess ordförande 1968–69, förbundsordförande i Svenska Insjöfiskarenas Centralförbund 1965–69 samt styrelseledamot i Svenska Handelsbanken 1971-76."
        p7.imageOfMinister = "thorbjorn.jpg"
        p7.whatKindOfParti = "C"
        people.append(p7)
        
        let p8 = personClass()
        p8.namesOfMinister = " Olof Palme"
        p8.ageBetweenOfMinister = "1969 - 1976 & 1982 - 1986"
        p8.descriptionOfMinister = "Olof Palme föddes i Svea Artilleriregementes församling på Östermalm i Stockholm och växte upp i familjens våning på Östermalmsgatan 36. Hans föräldrar var Gunnar Palme (1886–1934), verkställande direktör för försäkringsbolaget Thule, liksom farfar Sven, och Elisabeth von Knieriem (1890–1972). Farmor Hanna von Born var av finlandssvensk adel och mor Elisabeth von Knieriem hörde till en balttysk köpmannafamilj. Hennes farfarsfar Alexander von Knieriem adlades 1814 av den ryske tsaren. Elisabeth von Knieriem föddes i Livland och kom till Sverige som flykting 1915."
        p8.imageOfMinister = "olof.jpg"
        p8.whatKindOfParti = "S"
        people.append(p8)
        
        let p9 = personClass()
        p9.namesOfMinister = "Tage Erlander"
        p9.ageBetweenOfMinister = "1946 - 1969"
        p9.descriptionOfMinister = "Erlander var son till organisten Erik Gustaf Erlander (1859–1936) och Alma Nilsson (1869–1961). Han var det tredje barnet av fyra; hans syskon var Janne (1893–1912), Anna (1894–1972) och Dagmar (1904–1988). Erlanders släkt på hans mormors sida härstammade från så kallade svedjefinnar som under 1600-talet inflyttade till Värmland indirekt från landskapet Savolax i Finland. Släkten hette från början Suhoinen. Tage Erlanders far hette Andersson från början – efter sin far Anders Erlandsson, men ändrade namnet till Erlander."
        p9.imageOfMinister = "tage.jpg"
        p9.whatKindOfParti = "S"
        people.append(p9)
        
        let p10 = personClass()
        p10.namesOfMinister = "Axel Pehrsson"
        p10.ageBetweenOfMinister = "1936"
        p10.descriptionOfMinister = "Axel Pehrsson (från 1937 Pehrsson-Bramstorp) föddes i Öja i Skåne, som son till förvaltaren Jacob Pehrsson och Ingrid. Han fick ta över ansvaret för gården i Lilla Isie socken som 14-åring, sedan fadern begått självmord efter att ha fått besked om dödlig cancersjukdom. Han kunde efter sitt giftermål tolv år senare köpa den närbelägna gården Bramstorp. Familjen flyttade tillbaka till arrendegården efter ett tiotal år."
        p10.imageOfMinister = "axel.jpg"
        p10.whatKindOfParti = "B"
        people.append(p10)
        
        
        let p11 = personClass()
        p11.namesOfMinister = "Axel Pehrsson"
        p11.ageBetweenOfMinister = "1936"
        p11.descriptionOfMinister = "Olof (från 1937 Pehrsson-Bramstorp) föddes i Öja i Skåne, som son till förvaltaren Jacob Pehrsson och Ingrid. Han fick ta över ansvaret för gården i Lilla Isie socken som 14-åring, sedan fadern begått självmord efter att ha fått besked om dödlig cancersjukdom. Han kunde efter sitt giftermål tolv år senare köpa den närbelägna gården Bramstorp. Familjen flyttade tillbaka till arrendegården efter ett tiotal år."
        p11.imageOfMinister = "olof.jpg"
        p11.whatKindOfParti = "S"
        people.append(p11)
        
        
        
        
       
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return people.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellgogo", for: indexPath) as!
        cellTableViewCell
        
        let currentPerson = people[indexPath.row]
        
        cell.nameLabel.text = currentPerson.namesOfMinister
        
        cell.ageLabel.text = currentPerson.ageBetweenOfMinister
        
        cell.theImage.image = UIImage(named: currentPerson.imageOfMinister)
        
        
        return cell
    }
    
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "gogoseg", sender: indexPath.row)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let dest = segue.destination as! ViewController
        
        dest.person = people[sender as! Int]
        
        dest.title = people[sender as! Int].namesOfMinister
                
    }
    
    
    
    }
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


